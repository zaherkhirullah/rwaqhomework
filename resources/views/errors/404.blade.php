@extends('errors::layout')

@section('title',   '404 page not found')

@section('message', \Lang::get('lang.m_404_page') )
