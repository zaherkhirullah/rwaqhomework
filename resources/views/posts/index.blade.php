@extends("layouts.app")
@section("content")

<div class="container">
       
    <div class="panel panel-body">
        <a href="{{route('posts.create')}}" class="btn btn-success">
            <i class="fa fa-plus"></i> 
            Add New post
        </a>
    </div>
    <table  class="table table-hover table-bordered">
        <thead> 
            <tr>
                <th>id</th>
                <th>title</th>
                <th>description</th>
                <th>Published By</th>
                <th>created date</th>
                <th>updated date</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->description}}</td>
                    @if(Route::is("posts.myPosts"))
                    <td>Me</td>    
                    @else
                    <td>{{$post->user->username}}</td>                    
                    @endif
                    <td>{{$post->created_at}}</td>
                    <td>{{$post->updated_at}}</td>
                    <td>
                        <a href="{{route('posts.show',$post->id)}}" title="View" class="btn btn-success">
                            <i class="fa fa-eye"></i>
                        </a>    
                        <a href="{{route('posts.edit',$post->id)}}" title="Edit" class="btn btn-primary">
                            <i class="fa fa-edit" ></i>
                        </a>    
                        <a href="{{route('posts.delete',$post->id)}}" title="delete" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </a>    
                    </td>
                </tr>
                @endforeach    
            
        </tbody>
    </table>
</div>
@endsection