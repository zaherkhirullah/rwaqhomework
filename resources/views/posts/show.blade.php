@extends('layouts.app')

@section('content')
 

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header "> {{$post->title}} Post Details </div>

               <div>
                   <ul class="unstyled-list">
                    <li>
                        <b class ="text-danger" > Title :    </b> 
                    <i class="text-primary">{{$post->title}} </i>
                    </li>
                    <li>
                        <b class ="text-danger" > Descript :      </b> 
                        <i class="text-primary">{{$post->description}}</i>
                    </li>
                    <li>
                        <b class ="text-danger" > Published By :</b> 
                        <i class="text-primary">{{$post->user->username}}</i>
                    </li>
                    <li><b class ="text-danger" > Created date :   </b> 
                        <i class="text-primary">{{$post->created_at}} </i>
                    </li>
                    <li>
                        <b class ="text-danger" > Updated date :</b> 
                        <i class="text-primary">{{$post->created_at}}    </i>
                    </li>
                   </ul>
                   <div class="pull-right">
                    <ul class="list-unstyled list-inline">
                    <li>
                                <a href="{{route('posts.edit',$post->id)}}"  class="btn btn-success">Edit</a>
                    </li>    
                        <li>
                        <form action="{{route('posts.destroy',$post->id)}}" method="post">
                            <button type="submit" class="btn btn-danger">  
                                <i class="fa fa-trash"></i>
                                 @lang('lang.delete')</button>   
                  
                        </form>
                
                        </li>
                    </ul>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>



@endsection