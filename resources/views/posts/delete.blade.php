@extends('layouts.app')

@section('content')
 

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header "> {{$post->title}} Post Details </div>
                   <center>
                       <h3> Are You Sur You Want delete this Post </h3>
                   </center>
                    <ul class="unstyled-list">
                    <li>
                        <b class ="text-danger" > Title :    </b> 
                    <i class="text-primary">{{$post->title}} </i>
                    </li>
                    <li>
                        <b class ="text-danger" > Descript :      </b> 
                        <i class="text-primary">{{$post->description}}</i>
                    </li>
                    <li>
                        <b class ="text-danger" > Published By :</b> 
                        <i class="text-primary">{{$post->user->username}}</i>
                    </li>
                    <li><b class ="text-danger" > Created date :   </b> 
                        <i class="text-primary">{{$post->created_at}} </i>
                    </li>
                    <li>
                        <b class ="text-danger" > Updated date :</b> 
                        <i class="text-primary">{{$post->created_at}}    </i>
                    </li>
                   </ul>
                   <form action="{{route('posts.destroy',$post->id)}}" method="PATCH">
                    @csrf   
                    {{ method_field('delete') }} 
                       <div class="modal-footer">
                                <button id="btn-delete" class="btn btn-rounded  pull-right btn-success" type="submit">
                                    <i class="fa fa-trash"></i> @lang('lang.delete')
                                </button>
                          </div>
                    </form>
               </div>
            </div>
        </div>
    </div>
</div>



@endsection