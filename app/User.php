<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $fillable = [
      'first_name','last_name','username','email',
      'confirm_email','password','role','isDeleted',
     ];
     protected $hidden = [
        'password', 'remember_token',
    ];
  
      public function Uid()
      {
        return Auth::id();
      } 
    
      public function posts()
      {
          return $this->hasMany('App\Post','user_id');
      }
      
     // list All users
     public function users()
     {
      return $this->where([['isDeleted','0'],['role_id','<>','1']])->orderBy('created_at','desc');
     }
    // list of  users has been deleted and list (Desc) by create date
     public function deletedusers()
     {
      return $this->where(['isDeleted','1'],['role_id','<>','1'])->orderBy('updated_at','desc');
     }

    
}
