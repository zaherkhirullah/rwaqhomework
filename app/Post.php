<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{ 

    protected $table = 'posts';
    protected $fillable = [
      'title','description','isDeleted','user_id',
     ];
    // list All users
    public function All_Posts()
    {
     return $this->all();
    }
    public function All_Posts_desc()
    {
     return $this->orderBy('created_at','desc');
    }
    public function User_Posts_desc($uid)
    {
     return $this->where('user_id',$uid)->orderBy('created_at','desc');
    }
    
    public function user()
    {
        return $this->belongsTo( 'App\User', 'user_id' );
    }
}
