<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use App\User;

use Session;
use Illuminate\Http\Request;
use App\Http\Requests\PostValidation;

class PostController extends Controller
{
    public function index()
    { 
        // get object from POST model 
            $post_model = new Post; 
        /**
         *  call All_Posts_desc Function  in Post Model 
         *  to display all posts with desc created date
         */
            $posts =  $post_model->All_Posts_desc()->get(); 
        
            return view("posts.index",compact("posts"));
        /**   OR
         *  return view("posts.index")->withPosts($posts);
         */
    
    }
    
    public function userPosts($id)
    { 
        $user = User::findOrfail($id);
        // get object from POST model 
            $post_model = new Post; 
        /**
         *  call All_Posts_desc Function  in Post Model 
         *  to display all posts with desc created date
         */
            $posts =  $post_model->User_Posts_desc($user->id)->get(); 
        
            return view("posts.index",compact("posts"));
            /**   OR
             *  return view("posts.index")->withPosts($posts);
             */
        
    }

    public function create()
    {
        return view("posts.create");
    }

    public function store(PostValidation $request)
    {

        $post  = new Post;
        $post->user_id = Auth::id();
        /* First way */
        // $post->title = $request->title;
        // $post->description =$request->description;

        /* OR  Second way */
        $post = $post->fill($request->all());
        
        /* OR  Third way */
        //Post::create($request->all());
        
        $post->save();
        Session::flash('success'," Successfuly created " .$post->title." post");
        return redirect()->route('posts.index');
    }

    
    public function show(Post $post)
    {
        return view("posts.show",compact('post'));
    }

    public function edit(Post $post)
    {
        return view("posts.edit", compact('post'));
    }

   
    public function update(Request $request, Post $post)
    {
        /* First way */
        // $post->title = $request->title;
        // $post->description =$request->description;

        /* OR  Second way */
         $post->update($request->all());    
       
        return redirect()->route('posts.index');
    }
    public function delete(Post $post)
    {
        return view("posts.delete", compact('post'));
    }
    public function destroy(Post $post)
    {   
        $name= $post->title;
        $post = Post::find($post)->first();
        $file->delete();
        Session::flash('success',' Sucessfully deleted the ' .$name . ' post .');
        return redirect()->route('posts.index');
    }
}
