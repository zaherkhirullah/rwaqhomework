<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('username',255)->unique();
            $table->string('email',255)->unique();
            $table->boolean('confirm_email')->default(0);            
            $table->string('password',255);
            $table->ipAddress('ip')->nullable();     
            $table->boolean('isDeleted')->default(0);
            $table->rememberToken();
            $table->timestamps();      
            $table->engine = 'InnoDB';
        });
     

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           
        Schema::dropIfExists('users'); 
    }
}
