<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        $role = new App\Http\Models\role();
        $role->slug= 'admin';
        $role->name= 'Admin';
        $role->save();
        $role = new App\Http\Models\role();
        $role->slug= 'User';
        $role->name= 'User';
        $role->save();
        $role = new App\Http\Models\role();
        $role->slug= 'manager';
        $role->name= 'Manager';
        $role->save();
    }
}
