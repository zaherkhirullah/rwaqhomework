<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->first_name='f_admin';
        $user->last_name='l_admin';
        $user->username='admin';
        $user->email='admin@admin.com';
        $user->password=bcrypt('admin');
        $user->save();
    }
}
