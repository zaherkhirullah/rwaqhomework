<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('homepage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/Posts/{id}', 'PostController@userPosts')->name('posts.myPosts');
Route::get('/Posts/delete/{post}', 'PostController@delete')->name('posts.delete');
Route::resource('/posts', 'PostController');
